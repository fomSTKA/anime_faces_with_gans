import ipywidgets as widgets
import webbrowser
import common.common as cls_common
import torchvision.utils as vutils
import webbrowser
from ipywidgets import Button, HBox, VBox, Textarea

class ClsBuilderBase:
    """Construct Output Panel for:
    0=DCGAN, 1=WGAN, 2=WGANGP"""
    def __init__(self, procress, f_imgshow):
        self._cls_progress = procress.neuronal_net_structure
        self._output = widgets.Output()
        self._model_to_load = ''
        self._imgshow = f_imgshow

        lay = {'width': '800px'}
        self._epochs_slider = widgets.IntSlider(layout=lay, value=self._cls_progress.epochs, min=1, 
                                     max=1000, step=1, 
                                     continuous_update=False, 
                                     description='Epochs')
        self._dropdown_files = widgets.Dropdown(options='none')
        
        self._box_autosave_mdl = widgets.Checkbox(False, description='autosave model')
        self._box_to_tensorboard = widgets.Checkbox(False, description='write to Tensorboard')        
        self._box_img = widgets.Checkbox(False, description='show epoch pics')
        
        self._tx_vobox = widgets.Text(value=str(self._cls_progress), 
                                      description='comment:',
                                      disabled=False)
        
        self._textarea = Textarea(layout={'heigth': '100%'},
                                 description='add. notes:',
                                 disable=False)

        self._radio_btn_plt = widgets.RadioButtons(
            style={'description_width': 'initial'},
            options=[('plot',0),('summary',1),('summ. & iter.',2)],
            description='to output:',
            disabled=False,)
        
        self._btn_train_start = Button(description='start training')
        self._btn_train_load = Button(description='load model')
        self._btn_tensorboard = Button(description='start Tensorboard')
        self._btn_plot_images = Button(description='generate images')
        
        self._btn_train_start.on_click(self._my_training_process)
        self._btn_train_load.on_click(self._my_load_model)
        self._btn_tensorboard.on_click(self._my_tensorboard)
        self._btn_plot_images.on_click(self._my_gen_images)
        self._box_smooth_labels = widgets.Checkbox(False, description='smoothing labels')
        self._box_noise_labels = widgets.Checkbox(False, description='noisy labels')
    def _my_load_model(self, b):
        self._output.clear_output()
        with self._output:
            print(self._dropdown_files.value)
            path = self._dropdown_files.value
            self._cls_progress.load_model(path)
            self._epochs_slider.value = self._cls_progress.epochs
    def _my_tensorboard(self, b):
        with self._output:
            url = 'http://localhost:6006/'
            webbrowser.open(url)
            cls_common.run_tensorboard('./')
    def _my_gen_images(self, b):
        self._output.clear_output()
        with self._output:
            img_fake = self._cls_progress.get_fake_samples()
            out = vutils.make_grid(img_fake.cpu()[0:56])
            self._imgshow(out, "Fake Images")
        
class ClsBuilder(object):
    """Concrete representation of builded panel type."""
    def __init__(self, process, f_imgshow, builder):
        """Initializer."""
        #type injection
        self._builder = builder(process, f_imgshow)

    def __repr__(self):
        return '{}'.format(self._cls_progress)

    @property
    def builder(self):
        return self._builder
        
        