import ipywidgets as widgets
import torchvision.utils as vutils
import random
import numpy as np
import torch
import matplotlib.pyplot as plt
from IPython.display import clear_output
from ipywidgets import Button, HBox, VBox

class cls_builder_real_vs_fake:
    def __init__(self, real_data, nz, process, device, f_imshow):
        """nz = latent vector (i.e. size of generator input)"""
        self._real_data = real_data
        self._nz = nz
        self._is_true = 0
        self._device = device
        self._cls_progress = process
        self._imgshow = f_imshow
        self._output = widgets.Output()

    def _check(self, b):
        with self._output:
            if self._is_true <= 0:
                print('Real image.')
            else:
                print('Fake image.')
    def get_img_real(self, title):
        images = next(iter(self._real_data))
        out = vutils.make_grid(images[0][0:1])
        self._imgshow(out, title)
    def get_img_fake(self, title):
        img_fake = self._cls_progress.plot_fake_images(self._cls_progress.get_params.fake_label)
        out = vutils.make_grid(img_fake.cpu()[0:1])
        self._imgshow(out, title)
    def _next(self, b):
        self._output.clear_output()
        b.description = 'next'
        b.value = -1
        with self._output:
            self._output.clear_output()
            self._random_plot()
    def _random_plot(self):
        title = 'Real or Fake?'
        if random.randint(0, 1) >= 1:
            fake = self._cls_progress.print_single_fake_image(title)
            self._is_true = 1
        else:
            real = self.get_img_real(title)
            self._is_true = 0
    def draw_buttons(self, img_path = './samples'):
        btn_check = Button(description='check')
        btn_next = Button(description='start')

        ui_middle_containter = VBox([btn_check, btn_next])
        ui_container = HBox([ui_middle_containter])
        btn_next.on_click(self._next)
        btn_check.on_click(self._check)
        btn_save.on_click(self._save_plot)
        display(ui_container, self._output)
        
        