# Discriminator - Critic Code
import crosscutting.discriminators.discriminator as cls_critic_all
import crosscutting.discriminators.discriminator_dcgan as cls_critic_dcgan_bn
import crosscutting.discriminators.discriminator_dcgan_nobn as cls_critic_dcgan_nobn
import crosscutting.discriminators.discriminator_dcgan_instnorm as cls_critic_dcgan_instnorm


import common.weights as cls_weights
import numpy as np
import ipywidgets as widgets
from torchsummary import summary
from IPython.display import clear_output
from ipywidgets import HBox, VBox

class ClsNetDBuilder:
    """Create the Discriminator."""
    def __init__(self, image_size, ndf, nc, ngpu, device, activation_func, id_dis = 0, n_extra = 0, is_dropout = False):
        self._ndf = ndf
        self._nc = nc
        self._ngpu = ngpu
        self._device = device
        self._image_size = image_size
        lay = {'width': '500px'}
        self._extra_lay_slider = widgets.IntSlider(layout=lay, value=0, min=0, 
                                     max=10, step=1, 
                                     continuous_update=False, 
                                     description='Extra Layer')
        self._extra_lay_slider.value = n_extra
        self._radio_btn_Disc = widgets.RadioButtons(
                    style={'description_width': 'initial'},
                    options= [('Conv_BatchN_LReLu_Sigmoid',cls_critic_all.cls_critic_conv_bn_lrelu_sigmoid),
                             ('Critic DCGAN BatchNorm',cls_critic_dcgan_bn.cls_DCGAN),
                             ('Critic DCGAN no BatchNorm',cls_critic_dcgan_nobn.cls_DCGAN),
                             ('Critic DCGAN InstanceNorm',cls_critic_dcgan_instnorm.cls_DCGAN)],
                    description='net-D.:',
                    disabled=False,)
        self._radio_btn_actfunc = widgets.RadioButtons(
                    style={'description_width': 'initial'},
                    options= [('None',0),
                              ('Sigmoid',1)],
                    description='Acti.-Func.',
                    disabled=False,)
        self._radio_btn_actfunc.index = activation_func
        self._radio_btn_Disc.index = id_dis
        self._box_dropout = widgets.Checkbox(False, description='dropout')
        self._box_dropout.value = is_dropout
    @property
    def netD(self):
        return self._netD
    @property
    def id_dis(self):
        return self._radio_btn_Disc.index
    @property
    def is_dropout(self):
        return self._box_dropout.value
    @property
    def activation_func(self):
        return self._radio_btn_actfunc.value
    @property
    def n_extra_layers_d(self):
        return self._extra_lay_slider.value
    
    def _flatten(self, w, k=3, s=1, p=0, m=True):
        """
        Returns the right size of the flattened tensor after
            convolutional transformation
        :param w: width of image
        :param k: kernel size
        :param s: stride
        :param p: padding
        :param m: max pooling (bool)
        :return: proper shape and params: use x * x * previous_out_channels

        Example:
        r = flatten(*flatten(*flatten(w=100, k=3, s=1, p=0, m=True)))[0]
        self.fc1 = nn.Linear(r*r*128, 1024)
        """
        return int((np.floor((w - k + 2 * p) / s) + 1) / 2 if m else 1), k, s, p, m
    
    def _init(self, func, drop, actfunc):
        print(func)
        clear_output()
        n_extra_layers = self._extra_lay_slider.value
        act_func = True if actfunc >= 1 else False
        self._netD = func(isize=self._image_size, ndf=self._ndf, nc=self._nc, ngpu=self._ngpu, isdropout=drop, sigmoid=act_func, n_extra_layers=n_extra_layers).to(self._device)

        # Handle multi-gpu if desired
        if (self._device.type == 'cuda') and (self._ngpu > 1):
            self._netD = nn.DataParallel(netD, list(range(self._ngpu)))

        # Apply the weights_init function to randomly initialize all weights
        #  to mean=0, stdev=0.2.

        self._netD.apply(cls_weights.cls_weights.weights_init)
        
        self.print_summary()
    def print_summary(self):
        # generator summary
        summary(self._netD, (3, 64, 64))
        # Print the model
        print(self._netD)
    def draw(self):
        box = HBox([self._radio_btn_Disc, self._radio_btn_actfunc])
        leftb = VBox([self._extra_lay_slider, box])
        rightb = VBox([leftb, self._box_dropout])
        ui = HBox([rightb])
        out = widgets.interactive_output(self._init, {'func':self._radio_btn_Disc, 'drop':self._box_dropout, 'actfunc':self._radio_btn_actfunc})
        display(ui, out)