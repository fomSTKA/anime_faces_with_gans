import ipywidgets as widgets
import torch.nn as nn
import torch
from ipywidgets import HBox, VBox

def wasserstein_loss(output, label, printout=False):
    return torch.mean(output * label).view(1)

class ClsSetSomeNetParams:
    def __init__(self, netD, netG, optim_id = 0, loss_id = 0):
        self._netD = netD
        self._netG = netG
        self._optim_id = optim_id
        self._loss_id = loss_id
    @property
    def optim_id(self):
        return self._optim_id
    @property
    def loss_id(self):
        return self._loss_id
    @property
    def netD(self):
        return self._netD
    @property
    def netG(self):
        return self._netG
    @property
    def optimizerD(self):
        return self._optimizerD
    @property
    def optimizerG(self):
        return self._optimizerG
    @property
    def criterion(self):
        return self._criterion
    def _set_some_net_params(self, optim, loss):
        # Setup optimizers for both G and D
        self._optim_id = optim
        self._loss_id = loss
        if optim <= 0:
            self._optimizerD = torch.optim.Adam(self._netD.parameters(), lr=0.0002, betas=(0.5, 0.999))
            self._optimizerG = torch.optim.Adam(self._netG.parameters(), lr=0.0002, betas=(0.5, 0.999))
        elif optim <= 1:
            self._optimizerD = torch.optim.Adam(self._netD.parameters(), lr=0.0001, betas=(0.5, 0.9))
            self._optimizerG = torch.optim.Adam(self._netG.parameters(), lr=0.0001, betas=(0.5, 0.9))
        elif optim == 2:
            self._optimizerD = torch.optim.Adam(self._netD.parameters(), lr=0.0001, betas=(0, 0.9))
            self._optimizerG = torch.optim.Adam(self._netG.parameters(), lr=0.0001, betas=(0, 0.9))
        elif optim == 3:
            self._optimizerD = torch.optim.Adam(self._netD.parameters(), lr=0.00005, betas=(0.9, 0.999))
            self._optimizerG = torch.optim.Adam(self._netG.parameters(), lr=0.00005, betas=(0.9, 0.999))
        elif optim >= 4:
            self._optimizerD = torch.optim.RMSprop(self._netD.parameters(), lr=0.00005)
            self._optimizerG = torch.optim.RMSprop(self._netG.parameters(), lr=0.00005)

        # Initialize Loss function
        if loss == 0:
            self._criterion = wasserstein_loss
        else:
            self._criterion = nn.BCELoss()
            
    def draw(self):

        radio_btn_optim = widgets.RadioButtons(layout = {'width': '500px'},
                    style={'description_width': 'initial'},
                    options=[('Adam, lr=0.0002, b1=0.5, b2=0.999 (e.g. DCGAN)',0),
                             ('Adam, lr=0.0001, b1=0.5, b2=0.9 (e.g. DCGAN)',1),
                             ('Adam, lr=0.0001, b1=0, b2=0.9 (e.g. WGAN-GP)',2),
                             ('Adam, lr=0.00005, b1=0.9, b2=0.999 (e.g. DCGAN)',3),
                             ('RMSprop, lr=0.00005 (e.g. WGAN)',4)],
                    description='Optimizer:',
                    disabled=False,)
        radio_btn_optim.value = self._optim_id
        
        radio_btn_loss = widgets.RadioButtons(
                    style={'description_width': 'initial'},
                    options=[('Wasserstein',0),
                             ('BCE',1)],
                    description='Loss:',
                    disabled=False,)
        radio_btn_loss.value = self._loss_id

        output_optim = widgets.interactive_output(self._set_some_net_params, {'optim':radio_btn_optim, 'loss':radio_btn_loss})
        ui = HBox([radio_btn_loss, radio_btn_optim])
        display(ui, output_optim)