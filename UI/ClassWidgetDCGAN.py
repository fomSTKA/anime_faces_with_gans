import ipywidgets as widgets
import UI.ClassWidgetBuilderBase as cls_widget_base
import logic.ClassTensorBoard as cls_tensorboard
from os import listdir
from os.path import isfile, join
from ipywidgets import Button, HBox, VBox

class ClsBuilderDCGAN(cls_widget_base.ClsBuilderBase):
    def __init__(self, procress, f_imgshow):
        super().__init__(procress, f_imgshow)
        #self._box_gen_used = widgets.Checkbox(False, description='gen. labels used')
        
        self._box_autosave_mdl.value = self._cls_progress._bop['autosave']
        self._radio_btn_plt.value = self._cls_progress._bop['to_output']
        self._epochs_slider.value = self._cls_progress._bop['num_epochs']
        self._box_img.value = self._cls_progress._bop['img_per_epoch']
        self._box_to_tensorboard.value = self._cls_progress._bop['to_tensorboard']
        self._box_smooth_labels.value = self._cls_progress._bop['smooth_labels']
        self._box_noise_labels.value = self._cls_progress._bop['noisy_labels']
        
    def _my_training_process(self, b):
        self._output.clear_output()
        with self._output:
            self._cls_progress.to_output = self._radio_btn_plt.value
            self._cls_progress.autosave_mdl = self._box_autosave_mdl.value
            #set name and number of experiment folder:
            self._cls_progress.init_exp_folder()
            if self._box_to_tensorboard.value:
                name = '{}tensorboard/{}'.format(self._cls_progress.exp_folder, str(self._cls_progress))
                tb = cls_tensorboard.ClsTensorBoard(name, self._tx_vobox.value)
                self._cls_progress.tensorboard = tb.writer
            else:
                self._cls_progress.tensorboard = None

            #self._cls_progress.gen_on_off(self._box_gen_used.value)
            self._cls_progress.smooth_labels = self._box_smooth_labels.value
            self._cls_progress.noisy_labels = self._box_noise_labels.value
            self._cls_progress.epochs = self._epochs_slider.value
            self._cls_progress.imgs_per_epoch = self._box_img.value
            
            self._cls_progress.training_loop()
    def draw_buttons(self, path_saved_models):        
        self._dropdown_files=widgets.Dropdown(options=path_saved_models)
                
        upper_box = HBox([self._btn_train_load, self._btn_train_start, self._btn_tensorboard, self._btn_plot_images])
        checker_box = VBox([self._box_smooth_labels, self._box_noise_labels])
        left_tf_box = HBox([self._tx_vobox, self._box_to_tensorboard, self._box_img])
        first_row = HBox([self._dropdown_files, self._box_autosave_mdl])
        
        lower_row = HBox([checker_box, self._radio_btn_plt])
        left_box = VBox([first_row, left_tf_box, upper_box, self._epochs_slider])
        ui_container = VBox([left_box, lower_row])
        
        display(ui_container, self._output)
        
        