# Discriminator - Critic Code
import crosscutting.generators.generator_DCGAN as cls_gen_DCGAN
import crosscutting.generators.generator_DCGAN_nobn as cls_gen_DCGAN_nobn
import crosscutting.generators.generator_DCGAN_instnorm as cls_gen_DCGAN_instnorm
import common.weights as cls_weights
import ipywidgets as widgets
from torchsummary import summary
from IPython.display import clear_output
from ipywidgets import HBox, VBox

class ClsNetGenBuilder:
    """Create the Discriminator."""
    def __init__(self, image_size, nz, ngf, nc, ngpu, device, id_gen = 0, n_extra = 0):
        self._image_size = image_size
        self._ngf = ngf
        self._nz = nz
        self._nc = nc
        self._ngpu = ngpu
        self._device = device
        
        self._radio_btn_Disc = widgets.RadioButtons(
                    style={'description_width': 'initial'},
                    options= [('Generator DCGAN BatchNorm', cls_gen_DCGAN.cls_DCGAN),
                             ('Generator DCGAN no BatchNorm', cls_gen_DCGAN_nobn.cls_DCGAN),
                             ('Generator DCGAN InstanceNorm', cls_gen_DCGAN_instnorm.cls_DCGAN)],
                    description='net-Gen.:',
                    disabled=False,)
        self._radio_btn_Disc.index = id_gen
        lay = {'width': '500px'}
        self._extra_lay_slider = widgets.IntSlider(layout=lay, value=0, min=0, 
                                     max=10, step=1, 
                                     continuous_update=False, 
                                     description='Extra Layer')
        self._extra_lay_slider.value = n_extra

    @property
    def netG(self):
        return self._netG
    @property
    def id_gen(self):
        return self._radio_btn_Disc.index
    @property
    def n_extra_layers_g(self):
        return self._extra_lay_slider.value
    
    def _init(self, func):
        print(func)
        clear_output()
       
        n_extra_layers = self._extra_lay_slider.value
        self._netG = func(isize=self._image_size, nz=self._nz, ngf=self._ngf, nc=self._nc, ngpu=self._ngpu, n_extra_layers=n_extra_layers).to(self._device)

        # Handle multi-gpu if desired
        if (self._device.type == 'cuda') and (self._ngpu > 1):
            self._netG= nn.DataParallel(netG, list(range(self._ngpu)))

        # Apply the weights_init function to randomly initialize all weights
        w = cls_weights.cls_weights.weights_init
        
        self._netG.apply(w)
        
        self.print_summary()
    def print_summary(self):
        # generator summary
        dim = 64
        #(100,3,3)
        summary(self._netG, (self._nz, 4, 4))
        # Print the model
        print(self._netG)
    def draw(self):
        ui = VBox([self._extra_lay_slider, self._radio_btn_Disc])
        out = widgets.interactive_output(self._init, {'func':self._radio_btn_Disc})
        display(ui, out)