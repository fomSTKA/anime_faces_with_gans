import ipywidgets as widgets
import UI.ClassWidgetBuilderBase as cls_widget_base
import logic.ClassTensorBoard as cls_tensorboard
from os import listdir
from os.path import isfile, join
from ipywidgets import Button, HBox, VBox

class ClsBuilderWGANGP(cls_widget_base.ClsBuilderBase):
    def __init__(self, procress, f_imgshow):
        super().__init__(procress, f_imgshow)
        lay = {'width': '500px'}
        self._critic_slider = widgets.IntSlider(layout=lay, value=5, min=1, 
                                     max=20, step=1, 
                                     continuous_update=False, 
                                     description='Critic-count')
        self._radio_btn_gp = widgets.RadioButtons(
            style={'description_width': 'initial'},
            options=[('clip weights',0),('penalty',1)],
            description='GP-Type:',
            disabled=False,)
        self._radio_btn_gp.value = self._cls_progress._bop['lipschitz']
        self._critic_slider.value = self._cls_progress._bop['critic_count']
        self._box_autosave_mdl.value = self._cls_progress._bop['autosave']
        self._radio_btn_plt.value = self._cls_progress._bop['to_output']
        self._epochs_slider.value = self._cls_progress._bop['num_epochs']
        self._box_img.value = self._cls_progress._bop['img_per_epoch']
        self._box_to_tensorboard.value = self._cls_progress._bop['to_tensorboard']
        self._box_smooth_labels.value = self._cls_progress._bop['smooth_labels']
        self._box_noise_labels.value = self._cls_progress._bop['noisy_labels']

    def _my_training_process(self, b):
        self._output.clear_output()
        with self._output:
            self._cls_progress.Lipschitz = self._radio_btn_gp.value
            self._cls_progress.to_output = self._radio_btn_plt.value
            self._cls_progress.autosave_mdl = self._box_autosave_mdl.value
            self._cls_progress.autosave_mdl = self._box_autosave_mdl.value
            #set name and number of experiment folder:
            self._cls_progress.init_exp_folder()
            if self._box_to_tensorboard.value:
                name = '{}tensorboard/{}'.format(self._cls_progress.exp_folder, str(self._cls_progress))
                tb = cls_tensorboard.ClsTensorBoard(name, self._tx_vobox.value)
                self._cls_progress.tensorboard = tb.writer
                self._cls_progress.tensorboard.add_text(str(self._cls_progress),'{} # {} # {}'.format(str(self._cls_progress.netD),self._tx_vobox.value, self._textarea.value,0))
            else:
                self._cls_progress.tensorboard = None
            self._cls_progress.smooth_labels = self._box_smooth_labels.value
            self._cls_progress.noisy_labels = self._box_noise_labels.value
            self._cls_progress.epochs = self._epochs_slider.value
            self._cls_progress.imgs_per_epoch = self._box_img.value
            
            self._cls_progress.training_loop(self._critic_slider.value)
    def draw_buttons(self, path_saved_models):        
        self._dropdown_files=widgets.Dropdown(options=path_saved_models)
        
        first_row = HBox([self._dropdown_files, self._box_autosave_mdl])
        second_row = HBox([self._btn_train_load, self._btn_train_start, self._btn_tensorboard, self._btn_plot_images])
        third_row = HBox([self._tx_vobox, self._box_to_tensorboard, self._box_img])
        fourth_row = VBox([self._epochs_slider, self._critic_slider])
        checker_box = VBox([self._box_smooth_labels, self._box_noise_labels])
        five_row_mid = VBox([self._radio_btn_plt])
        five_row_right = VBox([self._radio_btn_gp])
        
        five_row = HBox([five_row_mid, five_row_right, checker_box])
        last_row = VBox([self._textarea], layout={'heigth': '400px', 'weight': '300px'})
        ui_container = VBox([first_row, third_row, second_row, fourth_row, five_row, last_row])
        
        display(ui_container, self._output)
        