import torch
import os

class cls_io_modules(object):
    def __init__(self, **kwargs):
        self._img_list = []
        self._G_losses = []
        self._D_losses = []
        self._epoch = 0
        self._optimizerD = kwargs['optimizerD']
        self._optimizerG = kwargs['optimizerG']
        self._model_path = kwargs['model_path']
        self._smooth_labels = False
        self._noisy_labels = False
        self._autosave_mdl = False
        self._smooth_labels = False
        self._noisy_labels = False
        self._is_gen = False
        self._lipschitz = 0
        self._to_output = 0
    
    @property
    def to_output(self):
        return self._to_output
    @to_output.setter
    def to_output(self, value):
        self._to_output = value

    @property
    def is_gen(self):
        return self._is_gen
    @is_gen.setter
    def is_gen(self, value):
        self._is_gen = value

    @property
    def smooth_labels(self):
        return self._smooth_labels
    @smooth_labels.setter
    def smooth_labels(self, value):
        self._smooth_labels = value
    
    @property
    def noisy_labels(self):
        return self._noisy_labels
    @noisy_labels.setter
    def noisy_labels(self, value):
        self._noisy_labels = value

    @property
    def autosave_mdl(self):
        return self._autosave_mdl
    @autosave_mdl.setter
    def autosave_mdl(self, value):
        self._autosave_mdl = value
    
    @property
    def smooth_labels(self):
        return self._smooth_labels
    @smooth_labels.setter
    def smooth_labels(self, value):
        self._smooth_labels = value
    
    @property
    def noisy_labels(self):
        return self._noisy_labels
    @noisy_labels.setter
    def noisy_labels(self, value):
        self._noisy_labels = value
        
    @property
    def model_path(self):
        return self._model_path

    @property
    def optimizerD(self):
        return self._optimizerD
    @optimizerD.setter
    def optimizerD(self, _optimizerD):
        self._optimizerD = _optimizerD

    @property
    def optimizerG(self):
        return self._optimizerG
    @optimizerG.setter
    def optimizerG(self, _optimizerG):
        self._optimizerG = _optimizerG
        
    @property
    def img_list(self):
        return self._img_list
    @img_list.setter
    def img_list(self, img_list):
        self._img_list = img_list

    @property
    def G_losses(self):
        return self._G_losses
    @G_losses.setter
    def G_losses(self, G_losses):
        self._G_losses = G_losses
        
    @property
    def D_losses(self):
        return self._D_losses
    @D_losses.setter
    def D_losses(self, D_losses):
        self._D_losses = D_losses
        
    @property
    def epochs(self):
        return self._epoch
    @epochs.setter
    def epochs(self, value):
        self._epoch = value
        
    @property
    def optD_lr(self):
        for param_group in self._optimizerD.param_groups:
            return param_group['lr']

    @property
    def optG_lr(self):
        for param_group in self._optimizerD.param_groups:
            return param_group['lr']
