import re
import numpy as np
import os
import common.common as cls_common
from torch.utils.tensorboard import SummaryWriter
from os import listdir
from os.path import isdir, join

class ClsTensorBoard(object):
    """Administration of TensorBoard."""
    def __init__(self, folder, comment = None):
        self._TBoard_writer = SummaryWriter(folder, comment = comment)
    @property
    def writer(self):
        return self._TBoard_writer
