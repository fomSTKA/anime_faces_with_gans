import logic.ClassModulesIO as cls_io_modules
import common.common as cls_common
import torch
import numpy as np
import matplotlib.pyplot as plt
import torchvision.utils as vutils
import os
import json
from os import listdir
from os.path import isfile, join
from IPython.display import clear_output
from numpy.random import choice
from numpy import ones
from numpy import zeros
from numpy.random import randn
from torch.autograd import Variable

#Dependency Injection
#Listing of injection for neuronal network structures.

class ClsNetTypes(cls_io_modules.cls_io_modules):
    """Basic class for neuronal networks."""
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._netD = kwargs['netD']
        self._netG = kwargs['netG']
        self._img_show = kwargs['f_img_show']
        self._exp_num = 'none'
        self._tensorboard = None
            
    def init_exp_folder(self):
        self._exp_base_folder = cls_common.experiment_folder()
        
    @property
    def imgs_per_epoch(self):
        return self._imgs_per_epoch
    @imgs_per_epoch.setter
    def imgs_per_epoch(self, value):
        self._imgs_per_epoch = value
        
    @property
    def exp_folder(self):
        return self._exp_base_folder

    @property
    def netG(self):
        return self._netG
    @netG.setter
    def netG(self, netG):
        self._netG = netG
        
    @property
    def netD(self):
        return self._netD
    @netD.setter
    def netD(self, netD):
        self._netD = netD
                
    @property
    def fixed_noise(self):
        return self._fixed_noise
    @fixed_noise.setter
    def fixed_noise(self, value):
        self._fixed_noise = value

    @property
    def dataloader(self):
        return self._dataloader
    @dataloader.setter
    def dataloader(self, value):
        self._dataloader = value
    
    @property
    def tensorboard(self):
        return self._tensorboard
    @tensorboard.setter
    def tensorboard(self, value):
        self._tensorboard = value
    
    @property
    def criterion(self):
        return self._criterion
    @criterion.setter
    def criterion(self, value):
        self._criterion = value
    
    @property
    def params(self):
        return self._bop
    @params.setter
    def params(self, value):
        self._bop = value

    @property
    def device(self):
        return self._device
    @device.setter
    def device(self, value):
        self._device = value
    
    def train(self):
        self._netD.train()
        self._netG.train()
        
    def eval(self):
        self._netD.eval()
        self._netG.eval()
                
    def Lipschitz(self, constraint):
        self._lipschitz = constraint
        
    def write_settings_json(self, type, critic_count = 0):
        """write out generator config to generate images together wth training checkpoints (.pth)"""
        if self.tensorboard != None:
            tb = True
        else:
            tb = False
        settings_config = {"type": type,
                            "imageSize": self._bop['image_size'],
                            "batch_size": self._bop['batch_size'],
                            "nc": self._bop['nc'],
                            "nz": self._bop['nz'],
                            "ndf": self._bop['ndf'],
                            "ngf": self._bop['ngf'], 
                            "ngpu": self._bop['ngpu'],
                            "activation_func": self._bop['activation_func'],
                            "n_extra_layers_g": self._bop['n_extra_layers_g'],
                            "n_extra_layers_d": self._bop['n_extra_layers_d'],
                            "workers": self._bop['workers'],
                            "optim": self._bop['optim'],
                            "loss_id": self._bop['loss_id'],
                            "id_gen": self._bop['id_gen'], 
                            "id_dis": self._bop['id_dis'],
                            "is_dropout": self._bop['is_dropout'],
                            "smooth_labels": self.smooth_labels,
                            "noisy_labels": self.noisy_labels,
                            "autosave": self._autosave_mdl,
                            "real_label": self._bop['real_label'],
                            "fake_label": self._bop['fake_label'],
                            "is_gen": self._is_gen,
                            "lipschitz": self._lipschitz,
                            "to_output": self.to_output,
                            "opt_net_struc": self._bop['opt_net_struc'],
                            "critic_count": critic_count,
                            "img_per_epoch": self.imgs_per_epoch,
                            "to_tensorboard": tb,
                            "num_epochs": self._epoch}
        os.system('mkdir {0}'.format(self._exp_base_folder))

        with open(os.path.join(self._exp_base_folder, "settings_config.json"), 'w') as gcfg:
            gcfg.write(json.dumps(settings_config)+"\n")

    def save_fake_images(self, iteration):
        os.system('mkdir {0}'.format(self._exp_base_folder))
        vutils.save_image(self.get_fake_samples(), '{0}/fake_samples_{1}.png'.format(self._exp_base_folder, iteration))

    def generated_latent_noise(batch_size, device):
        """Generate points in latent space as input for the generator."""
        noise = torch.FloatTensor().resize_(batch_size, 2)
        noise.normal_(0, 1)
        noise = Variable(noise.to(device))
        return noise
    
    def get_custom_file_name(self, epoch, comment):
        """Concatenate file name depends on
        settings."""
        file = 'exp_{}_'.format(comment)
        if self._smooth_labels:
            file = file + 'smooth_'
        if self._noisy_labels:
            file = file + 'noisy_'
        if self._is_gen:
            file = file + 'with_gen_'
        return '{}{:03d}.pt'.format(file, epoch)
    
    def save_model(self, epoch,  fully_path):
        file = 'epoch_{:d}_{}'.format(epoch, fully_path)
        print('save_model: {}'.format(fully_path))
        torch.save({
            'epoch':epoch,
            'model_G_state_dict':self._netG.state_dict(),
            'model_D_state_dict':self._netD.state_dict(),
            'optimizer_G_state_dict':self._optimizerG.state_dict(),
            'optimizer_D_state_dict':self._optimizerD.state_dict(),
            'img_list':self._img_list,
            'loss_G':self._G_losses,
            'loss_D':self._D_losses,
        }, fully_path)
        
    def load_model(self, full_path):
        print(full_path)
        checkpoint = torch.load(full_path, map_location=torch.device(self._device))
        self._netG.load_state_dict(checkpoint['model_G_state_dict'])
        self._netD.load_state_dict(checkpoint['model_D_state_dict'])
        self._optimizerG.load_state_dict(checkpoint['optimizer_G_state_dict'])
        self._optimizerD.load_state_dict(checkpoint['optimizer_D_state_dict'])
        self._G_losses = checkpoint['loss_G']
        self._D_losses = checkpoint['loss_D']
        self._epoch = checkpoint['epoch']
        self._img_list = checkpoint['img_list']
        self.eval()    
        
    def gen_on_off(self, use_for_gen):
        self._is_gen = use_for_gen
        
    def label_smoothing(self, label_tensor, min=0.7, max=1.2):
        """Use soft labels instead of hard labels.
           Values are slightly more or less than hard labels."""
        if self._smooth_labels:
            return (max-min)*torch.rand(label_tensor.shape) + min
        return label_tensor
    
    def label_noisy(self, label_tensor, diff_from = 1, probability=0.05):
        if self._noisy_labels:
            size_selection = int(probability * label_tensor.shape[0])
            changed_labels = choice([elem for elem in range(label_tensor.shape[0])], size=size_selection)
            label_tensor[changed_labels] = diff_from - label_tensor[changed_labels]
            return label_tensor
        return label_tensor
    
    def get_real_samples(self, dataloader, real_label, device):
        """Select real image samples."""
        inputs = next(iter(dataloader))
        X_reals = inputs[0].to(device)
        b_size = X_reals.size(0) 
        y_labels = torch.full((b_size,), real_label, device=device)
        return X_reals, y_labels
    
    def _choice_label(self, n_samples, label):
        if label < 0:
            return -ones((n_samples, label))
        elif label > 0:
            return ones((n_samples, label))
        return zeros((n_samples, label))
    
    def get_fake_samples(self, is_fixed_noise = True, transform_fakes = True):
        """Generate fake samples."""
        #set all required_grad=False
        with torch.no_grad():
            noise = self.fixed_noise
            if not is_fixed_noise:
                noise = torch.randn(self._bop['batch_size'], self._bop['nz'], 1, 1, device=self._device)
            
            X_fakes = self._netG(Variable(noise))
        if transform_fakes:
            X_fakes.data = X_fakes.data.mul(0.5).add(0.5)
        return X_fakes
    
    def netD_requires_grad_params(self, req_grad):
        for p in self._netD.parameters():
            p.requires_grad = req_grad # to avoid computation
    
    def netG_requires_grad_params(self, req_grad):
        for p in self._netG.parameters():
            p.requires_grad = req_grad # to avoid computation
            
    def write_to_tensorboard(self):
        """All informations write to ./runs
        for Tensorboard."""
        #DISCRIMINATOR
        images = next(iter(self._dataloader))
        img_grid = vutils.make_grid(images[0].to(self._device)[0:56])
        one_real_pic = vutils.make_grid(images[0].to(self._device)[0:1])
        # write discriminator to tensorboard
        img_real, real_labels = self.get_real_samples(self._dataloader, self._bop['real_label'], self._device)
        self._tensorboard.add_image('real_images', img_grid)
        self._tensorboard.add_image('one_real_image', one_real_pic)
        self._tensorboard.add_graph(self._netD, img_real)
        
        # write generator to tensorboard
        self._tensorboard.add_scalar('Loss generator from train', errG.item())
        self._tensorboard.add_scalar('Loss discriminator from train', errD.item())
            
        self._tensorboard.close()
        
    def print_single_fake_image(self, title):
        if self._imgs_per_epoch:
            img_fake = self.get_fake_samples(False, False)
            out = vutils.make_grid(img_fake.cpu()[0:1])
            self._img_show(out, title)
        
    def write_tensorboard_fake_images(self, title, img_count = 1, n_iter = None):
        """Create fake images from Generator-Model."""
        img_fake = self.get_fake_samples()
        out = vutils.make_grid(img_fake.cpu()[0:img_count])
        self._tensorboard.add_image(title, out, n_iter)
        
    def plot_losses(self):
        clear_output()
        plt.figure(figsize=(20,10))
        plt.title("Generator and Discriminator Loss During Training")
        plt.plot(self._G_losses,label="G")
        plt.plot(self._D_losses,label="C")
        plt.xlabel("iterations")
        plt.ylabel("Loss")
        plt.legend()
        plt.show()
        
class ClsNeuronalNetwork(object):
    """Concrete representation of deep learning
    neuronl network structures."""
    def __init__(self, nnet_type, bag_of_params, device, dataloader):
        """Initializer."""
        #network type injection
        self._nnet_type = nnet_type
        self._nnet_type.params = bag_of_params
        self._nnet_type.device = device
        self._nnet_type._dataloader = dataloader
        self._nnet_type.fixed_noise = torch.randn(bag_of_params['image_size'], bag_of_params['nz'], 1, 1, device=device)

    def __repr__(self):
        return '{}'.format(self._nnet_type)

    @property
    def neuronal_net_structure(self):
        return self._nnet_type