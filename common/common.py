import time
import os
import re
import numpy as np
from datetime import datetime
from os import makedirs
from os import listdir
from os.path import isdir, join

def timer(func, *args):
    def wrapper_time(*args):
        start_time = time.time()
        value = func(*args)
        print('++ Function {}() took {}s'.format(func.__name__,round(time.time() - start_time,2)))
        return value
    return wrapper_time

def print_model_state(network, optimizer):
    """Print model's state_dict"""
    print("Model's state_dict:")
    for param_tensor in network.state_dict():
        print(param_tensor, "\t", network.state_dict()[param_tensor].size())
    print('\n')

def create_folder_datetime(path):
    now = datetime.now
    new_folder = '{}/{}/'.format(path, now().strftime('%Y-%m-%d-%H:%M:%S'))

    # actually make the folder
    #./data/exp001/model/2020-01-20-15:29:24
    makedirs(os.path.dirname(new_folder), exist_ok = True)
    return new_folder

def run_tensorboard(prj_path):
    """Start tensorboard for that project and open new tab with it.
    Path example: /home/steffen/anpr02"""
    os.system('tensorboard --logdir={}/runs'.format(prj_path))

def experiment_folder(main_path = './data'):
    dirs = [f for f in listdir(main_path) if isdir(join(main_path, f))]
    res = [re.findall(r'\d+', dd) for dd in dirs if re.findall(r'\d+', dd)]
    res = np.array(res).flatten()
    exp_num = 1 + np.amax([int(i) for i in res])
    return '{}/exp{:03d}/'.format(main_path, exp_num)

