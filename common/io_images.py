import numpy as np
import matplotlib.pyplot as plt
import os
from tqdm import tqdm

class cls_io_images:
    def plot_images(self, rows, cols, img, title):
        grid = np.zeros(shape=(rows*64, cols*64, 3))
        for row in range(rows):
            #print('Row: {}'.format(row))
            for col in range(cols):
                grid[row*64:(row+1)*64, col*64:(col+1)*64, :] = img[row*cols + col]
                #print('img: {}'.format(row*col + col))
        plt.figure(figsize=(20,20))
        plt.imshow(grid)
        plt.title(title)
        plt.show()
        
    def load_images(self, path_to_image):
        loaded_data=[]
        for png in tqdm(os.listdir(path_to_image)):

            path='{}/{}'.format(path_to_image,png)
            if os.path.isfile(path):
                img=plt.imread(path)
                img=img.astype('float32')
                loaded_data.append(img)

        return np.array(loaded_data)