import numpy as np
import torch
import matplotlib.pyplot as plt
import os
import torchvision.utils as vutils
from torch.utils.tensorboard import SummaryWriter
from numpy.random import choice
from numpy import ones
from numpy import zeros
from numpy.random import randn

class cls_io_modules(object):
    def __init__(self, **kwargs):
        self._img_list = []
        self._G_losses = []
        self._D_losses = []
        self._netD = kwargs['netD']
        self._netG = kwargs['netG']
        self._epoch = 0
        self._optimizerD = kwargs['optimizerD']
        self._optimizerG = kwargs['optimizerG']
        self._model_path = './data/saved_models/'
        self._TFB_writer = None
        self._smooth_labels = False
        self._noisy_labels = False
        self._TF_board = False
        self._autosave_mdl = False
        self._smooth_labels = False
        self._noisy_labels = False
        self._is_gen = False
        self._lipschitz = 0
        self._to_output = 0
    
    @property
    def to_output(self):
        return self._to_output
    @to_output.setter
    def to_output(self, value):
        self._to_output = value

    @property
    def is_gen(self):
        return self._is_gen
    @is_gen.setter
    def is_gen(self, value):
        self._is_gen = value

    @property
    def smooth_labels(self):
        return self._smooth_labels
    @smooth_labels.setter
    def smooth_labels(self, value):
        self._smooth_labels = value
    
    @property
    def noisy_labels(self):
        return self._noisy_labels
    @noisy_labels.setter
    def noisy_labels(self, value):
        self._noisy_labels = value

    @property
    def autosave_mdl(self):
        return self._autosave_mdl
    @autosave_mdl.setter
    def autosave_mdl(self, value):
        self._autosave_mdl = value
    
    @property
    def smooth_labels(self):
        return self._smooth_labels
    @smooth_labels.setter
    def smooth_labels(self, value):
        self._smooth_labels = value
    
    @property
    def noisy_labels(self):
        return self._noisy_labels
    @noisy_labels.setter
    def noisy_labels(self, value):
        self._noisy_labels = value
        
    @property
    def model_path(self):
        return self._model_path

    @property
    def optimizerD(self):
        return self._optimizerD
    @optimizerD.setter
    def optimizerD(self, _optimizerD):
        self._optimizerD = _optimizerD

    @property
    def optimizerG(self):
        return self._optimizerG
    @optimizerG.setter
    def optimizerG(self, _optimizerG):
        self._optimizerG = _optimizerG
        
    @property
    def img_list(self):
        return self._img_list
    @img_list.setter
    def img_list(self, img_list):
        self._img_list = img_list

    @property
    def G_losses(self):
        return self._G_losses
    @G_losses.setter
    def G_losses(self, G_losses):
        self._G_losses = G_losses
        
    @property
    def D_losses(self):
        return self._D_losses
    @D_losses.setter
    def D_losses(self, D_losses):
        self._D_losses = D_losses
        
    @property
    def netG(self):
        return self._netG
    @netG.setter
    def netG(self, netG):
        self._netG = netG
        
    @property
    def netD(self):
        return self._netD
    @netG.setter
    def netD(self, netG):
        self._netD = netD

    @property
    def TF_board(self):
        return self._TF_board
    @smooth_labels.setter
    def TF_board(self, value):
        self._TF_board = value        
        
    @property
    def epochs(self):
        return self._epoch
    @epochs.setter
    def epochs(self, value):
        self._epoch = value
        
    def TFB(self, comment):
        self._TFB_writer = SummaryWriter(comment = '_{}'.format(comment))
        
    def Lipschitz(self, constraint):
        self._lipschitz = constraint

    def plot_fake_images(self, fake_label, img_count = 128):
        img_fake = self.get_fake_samples(fake_label, img_count)
        return img_fake
    
    def generated_latent_noise(batch_size, device):
        """Generate points in latent space as input for the generator."""
        noise = torch.FloatTensor().resize_(batch_size, 2)
        noise.normal_(0, 1)
        noise = Variable(noise.to(device))
        return noise
    def get_custom_file_name(self, epoch):
        """Concatenate file name depends on
        settings."""
        file = "exp_DCGAN_"
        if self._smooth_labels:
            file = file + 'smooth_'
        if self._noisy_labels:
            file = file + 'noisy_'
        if self._is_gen:
            file = file + 'with_gen_'
        
        return '{}{:3d}.pt'.format(file, epoch)
        
    def save_model(self, epoch, fully_path):
        file = 'epoch_{:d}_{}'.format(epoch, fully_path)
        torch.save({
            'epoch':epoch,
            'model_G_state_dict':self._netG.state_dict(),
            'model_D_state_dict':self._netD.state_dict(),
            'optimizer_G_state_dict':self._optimizerG.state_dict(),
            'optimizer_D_state_dict':self._optimizerD.state_dict(),
            'img_list':self._img_list,
            'loss_G':self._G_losses,
            'loss_D':self._D_losses,
        }, fully_path)
    def load_model(self, filename):
        print(self._model_path + '/' + filename)
        checkpoint = torch.load(self._model_path + '/' + filename, map_location=torch.device(self._device))
        self._netG.load_state_dict(checkpoint['model_G_state_dict'])
        self._netD.load_state_dict(checkpoint['model_D_state_dict'])
        self._optimizerG.load_state_dict(checkpoint['optimizer_G_state_dict'])
        self._optimizerD.load_state_dict(checkpoint['optimizer_D_state_dict'])
        self._G_losses = checkpoint['loss_G']
        self._D_losses = checkpoint['loss_D']
        self._epoch = checkpoint['epoch']
        self._img_list = checkpoint['img_list']
        self._netG.eval()
        self._netD.eval()                
    def gen_on_off(self, use_for_gen):
        self._is_gen = use_for_gen
    def label_smoothing(self, label_tensor, min=0.7, max=1.2):
        """Use soft labels instead of hard labels.
           Values are slightly more or less than hard labels."""
        if self._smooth_labels:
            return (max-min)*torch.rand(label_tensor.shape) + min
        return label_tensor
    def label_noisy(self, label_tensor, probability=0.05):
        if self._noisy_labels:
            size_selection = int(probability * label_tensor.shape[0])
            changed_labels = choice([elem for elem in range(label_tensor.shape[0])], size=size_selection)
            label_tensor[changed_labels] = 1 - label_tensor[changed_labels]
            return label_tensor
        return label_tensor
    def get_real_samples(self, dataloader, real_label, device):
        """Select real image samples."""
        inputs = next(iter(dataloader))
        X_reals = inputs[0].to(device)
        b_size = X_reals.size(0) 
        y_labels = torch.full((b_size,), real_label, device=device)
        return X_reals, y_labels
    def _choice_label(self, n_samples, label):
        if label < 0:
            return -ones((n_samples, label))
        elif label > 0:
            return ones((n_samples, label))
        return zeros((n_samples, label))
    def get_fake_samples(self, n_samples, fake_label):
        """Generate fake samples."""
        noise = torch.randn(64, 100, 1, 1, device=self._device)
        with torch.no_grad():
            X_fakes = self._netG(noise).detach().cpu()
        #y_labels = self._choice_label(n_samples, fake_label)
        return X_fakes#, y_labels
    def netD_requires_grad_params(self, req_grad):
        for p in self._netD.parameters():
            p.requires_grad = req_grad # to avoid computation

        
