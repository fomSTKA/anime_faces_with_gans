import logic.ClassNetTypes as cls_net_types
import common.common as cls_common
import time
import torch
import torchvision.utils as vutils
import datetime
from tqdm.notebook import tqdm
from torch.autograd import Variable

class ClsDCGAN(cls_net_types.ClsNetTypes):
    """Deep Convulution GAN"""
    def __init__(self, **kwargs):
        super().__init__(model_path = './data/saved_models/', **kwargs)
        self._criterion = kwargs['criterion']
        
    def __str__(self):
        return 'DCGAN'
    
    def training_loop(self):
        try:
            self.write_settings_json( str(self), 0)
            iters = 0
            start_time = time.time()
            path_from_timestamp = ""
            print("Starting Training Loop...for {} epochs.".format(self._epoch))

            num_save_epoch = 100
            data_length = len(self._dataloader)
            epochbar = tqdm(desc = 'epochs', total = self._epoch * data_length)
            pbar = tqdm(total = data_length)
            for epoch in range(self._epoch):
                # For each batch in the dataloadera
                epochbar.set_description(desc = 'data epoch [{:02d}/{:02d}]'.format(epoch + 1, self._epoch))
                pbar.set_description(desc = 'data epoch {:2d}'.format(epoch + 1))
                pbar.refresh()
                pbar.reset()
                epoch_is_saved = False
                for n_iter, data in enumerate(self._dataloader, 0):
                    ############################
                    # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
                    ###########################
                    ## Train with all-real batch
                    self.netD.zero_grad()
                    
                    # Format batch
                    real_cpu = data[0].to(self._device)
                    b_size = real_cpu.size(0)
                    label = torch.full((b_size,), self._bop['real_label'], device=self._device)
                    # smoothing labels 0.7...1.2
                    label = self.label_smoothing(label).to(self._device)
                    # Forward pass real batch through D
                    output = self.netD(real_cpu).view(-1)
                    # noisy labels
                    output_clone = self.label_noisy(output.clone())
                    # Calculate loss on all-real batch
                    errD_real = self._criterion(output_clone, label)
                    # Calculate gradients for D in backward pass
                    errD_real.backward()
                    D_x = output.mean().item()

                    ## Train with all-fake batch
                    # Generate batch of latent vectors
                    noise = torch.randn(b_size, self._bop['nz'], 1, 1, device=self._device)
                    # Generate fake image batch with G
                    fake = self.netG(noise)
                    label.fill_(self._bop['fake_label'])
                    label = self.label_smoothing(label, 0, 0.3).to(self._device)
                    # Classify all fake batch with D
                    output = self.netD(fake.detach()).view(-1)
                    # noisy labels
                    output_clone = self.label_noisy(output.clone())
                    # Calculate D's loss on the all-fake batch
                    errD_fake = self._criterion(output_clone, label)
                    # Calculate the gradients for this batch
                    errD_fake.backward()
                    D_G_z1 = output.mean().item()
                    # Add the gradients from the all-real and all-fake batches
                    errD = errD_real + errD_fake
                    # Update D
                    self._optimizerD.step()

                    ############################
                    # (2) Update G network: maximize log(D(G(z)))
                    ###########################
                    # Generator update
                        
                    self.netG.zero_grad()
                    label.fill_(self._bop['real_label'])  # fake labels are real for generator cost
                        
                    # Since we just updated D, perform another forward pass of all-fake batch through D
                    output = self.netD(fake).view(-1)
                    # noisy labels
                    # Calculate G's loss based on this output
                    errG = self._criterion(output, label)
                    # Calculate gradients for G
                    errG.backward()
                    D_G_z2 = output.mean().item()
                    
                    if self._to_output == 2:
                        print(f'Iteration: {iters}, Generator Cost: {errG.item()}, Critic Cost: {errD.item()}')
                    # Update G
                    self._optimizerG.step()
                    if self.tensorboard != None:
                        self.tensorboard.add_scalars('train',{'Loss D real':errD_real.item(), 
                                                              'Loss D fake':errD_fake.item()}, iters)
                        self.tensorboard.add_scalars('train',{'Loss G':errG.item(), 
                                                              'Loss D':errD.item()}, iters)
                        self.tensorboard.add_scalars('train',{'D_x':D_x, 
                                                              'D_G_z1':D_G_z1,
                                                              'D_G_z2':D_G_z2}, iters)

                    if self._to_output <= 0:
                        self.plot_losses()
                        
                    # Output training stats
                    if n_iter % 150 == 0:
                        if self._to_output >= 1:
                            print('[{:03d}/{:03d}][{:03d}/{:03d}]\tLoss_D: {:9.4f}\tLoss_G: {:9.4f}\tD(x): {:9.4f}\tD(G(z)): {:9.4f} / {:9.4f}'.format(epoch, self._epoch, n_iter, len(self._dataloader),
                                     errD.item(), errG.item(), D_x, D_G_z1, D_G_z2))

                    # Save Losses for plotting later
                    self._G_losses.append(errG.item())
                    self._D_losses.append(errD.item())

                    # Check how the generator is doing by saving G's output on fixed_noise
                    if (iters % num_save_epoch == 0) or ((epoch == self._epoch-1) and 
                                              (n_iter == len(self._dataloader)-1)):
                        if self.tensorboard != None:
                            self.write_tensorboard_fake_images('generated fake images', 56, iters)
                            self.write_tensorboard_fake_images('generated single fake image', 1, iters)

                        #appand images from generator
                        self._img_list.append(vutils.make_grid(self.get_fake_samples(), padding=2, normalize=True))

                    iters += 1
                    pbar.update(1)
                    epochbar.update(1)

                loc_epo = self._epoch - 1

                if self._to_output >= 1:
                    print("Epoch {:03d}....duration time {}s".format(loc_epo, datetime.timedelta(seconds=round(time.time() - start_time,0))))

                if path_from_timestamp == "":
                    path_from_timestamp = cls_common.create_folder_datetime(self.exp_folder + self._model_path)
                
                # every epoch x print image
                if ((epoch + 1) % 25 <= 0):
                    # appand images from generator every x epoch
                    self._img_list.append(self.get_fake_samples())
                    self.print_single_fake_image('Epoch {:03d}'.format(loc_epo))
                    if self._autosave_mdl:
                        epoch_is_saved = True
                        self.save_fake_images(epoch + 1)    
                        self.save_model(epoch + 1, path_from_timestamp + '/' + self.get_custom_file_name(epoch + 1, 'DCGAN_' + str(self.netD)))
            
            if not epoch_is_saved:
                self.print_single_fake_image('Epoch {:03d}'.format(epoch + 1))
                self.save_model(self._epoch, path_from_timestamp + '/' + self.get_custom_file_name(self._epoch, 'DCGAN_' + str(self.netD)))
            print("Training done....duration time {}s".format(datetime.timedelta(seconds=round(time.time() - start_time,0))))
            pbar.close()
            epochbar.close()
        finally:
            if self.tensorboard != None:
                self.tensorboard.close()