import logic.ClassNetTypes as cls_net_types
import numpy as np
import matplotlib.pyplot as plt
import torch
import torchvision.utils as vutils
import time
from IPython.display import display
from torch import autograd
import datetime
from tqdm.notebook import tqdm
import common.common as cls_common

from numpy.random import random
from numpy import mean
from torch.autograd import Variable

class ClsWGAN_GP(cls_net_types.ClsNetTypes):
    def __init__(self, **kwargs):
        """Lists to keep track of progress""" 
        super().__init__(model_path = '/model/', **kwargs)
        self._clip = 0.01
        self._criterion = kwargs['criterion']
    def __str__(self):
        return 'WGAN_GP'
    
    def clip_netD_weights(self):
            for p in self.netD.parameters():
                p.data.clamp_(-self._clip, self._clip)
    
    def gradient_penalty(self, real_data, fake_data, device):
        n_elements = real_data.nelement()
        batch_size = real_data.size()[0]

        colors = real_data.size()[1]
        image_width = real_data.size()[2]
        image_height = real_data.size()[3]
        alpha = torch.rand(batch_size, 1)
        
        alpha = alpha.expand(batch_size, int(n_elements / batch_size)).contiguous()
        alpha = alpha.view(batch_size, colors, image_width, image_height)
        alpha = alpha.to(device)

        fake_data = fake_data.view(batch_size, colors, image_width, image_height)
        interpolates = alpha * real_data.detach() + ((1 - alpha) * fake_data.detach())

        interpolates = interpolates.to(device)
        interpolates.requires_grad_(True)
        critic_interpolates = self._netD(interpolates)

        gradients = autograd.grad(
            outputs=critic_interpolates,
            inputs=interpolates,
            grad_outputs=torch.ones(critic_interpolates.size()).to(device),
            create_graph=True,
            retain_graph=True,
            only_inputs=True
        )[0]

        gradients = gradients.view(gradients.size(0), -1)
        gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean() * self._bop['penalty_weight']
        return gradient_penalty 
    
    def training_loop(self, critic_count = 5):
        try:
            self.write_settings_json( str(self), critic_count)
            start_time = time.time()
            path_from_timestamp = ""
            num_save_iters = 1
            gen_iterations = 0
            iters = 0
            # The numbers 0 and -1
            print("Starting Training Loop...for {} epochs.".format(self._epoch))
            torch.autograd.set_detect_anomaly(True)
            #init tensors
            input = torch.FloatTensor(self._bop['batch_size'], 3, self._bop['image_size'], self._bop['image_size'])
            one = torch.FloatTensor([self._bop['real_label']])
            mone = torch.FloatTensor([self._bop['fake_label']])
            
            noise = torch.FloatTensor(self._bop['batch_size'], self._bop['nz'], 1, 1)
            fixed_noise = torch.FloatTensor(self._bop['batch_size'], self._bop['nz'], 1, 1).normal_(0, 1)
            
            #if cuda
            self.netD.to(self._device)
            self.netG.to(self._device)
            input = input.to(self._device)
            one, mone = one.to(self._device), mone.to(self._device)
            noise, self._fixed_noise = noise.to(self._device), self._fixed_noise.to(self._device)

            #init processbar
            data_length = len(self._dataloader)
            epochbar = tqdm(desc = 'epochs', total = self._epoch * data_length)
            pbar = tqdm(total = data_length)

            for epoch in range(self._epoch):
                data_iter = iter(self._dataloader)
                
                epoch_is_saved = False
                epochbar.set_description(desc = 'data epoch [{:02d}/{:02d}]'.format(epoch + 1, self._epoch))
                pbar.set_description(desc = 'data epoch {:02d}'.format(epoch + 1))
                pbar.refresh()
                pbar.reset()
                
                i = 0
                bla = 0
                while i < data_length:
                    # For each batch in the dataloadera
                    # more updates for critic than for generator
                    # train the discriminator Diters times
                    if gen_iterations < 25 or gen_iterations % 500 == 0:
                        Diters = 100
                    else:
                        Diters = critic_count

                    cc = 0
                    self.netD_requires_grad_params(True)
                    while cc < Diters and i < data_length:
                        cc += 1
                        iters += 1
                        ############################
                        # (1) Update C network -E[D(x)] + E[D(G(z))]
                        ###########################
                        if self.Lipschitz <= 0:
                            # Clipping
                            self.clip_netD_weights()
                        
                        data = data_iter.next()
                        i += 1                       
                        
                        ### train with real ###
                        real_cpu, _ = data
                        # set the gradients to zero
                        self.netD.zero_grad()
                        b_size = real_cpu.size(0)
                        
                        # smoothing labels 0.7...1.2
                        label = torch.full((b_size,), self._bop['real_label'], device=self._device)
                        #print('label before:')
                        #print(label)
                        label = self.label_smoothing(label).to(self._device)
                        #print('label after smoothing:')
                        #print(label)
                        
                        real_cpu = real_cpu.to(self._device)                        
                        input.resize_as_(real_cpu).copy_(real_cpu)
                        inputv = Variable(input)
                        output_real = self.netD(inputv).view(-1)
                        # noisy labels
                        output_real = self.label_noisy(output_real, 0)
                        #print('label behind noisey:')
                        #print(output_clone)

                        # Calculate loss on all-real batch
                        errD_real = self._criterion(output_real, label)
                        # Calculate gradients for D in backward pass
                        errD_real.backward()
                        
                        ## Train with all-fake batch
                        # Generate batch of latent vectors
                        noise.resize_(b_size, self._bop['nz'], 1, 1).normal_(0, 1)
                        with torch.no_grad():
                            noisev = Variable(noise) # totally freeze netG
                        fake = Variable(self.netG(noisev).data)
                        inputv = fake
                        label.fill_(self._bop['fake_label'])
                        label = self.label_smoothing(label, -1.3, -0.7).to(self._device)
                        
                        # Classify all fake batch with D
                        output_fake = self.netD(inputv).view(-1)
                        output_fake = self.label_noisy(output_fake, 0)
                        # Calculate D's loss on the all-fake batch
                        errD_fake = self._criterion(output_fake, label)
                        errD_fake.backward()
                        errD = errD_real - errD_fake
                        if self.Lipschitz == 1:
                            gradient_penalty = self.gradient_penalty(real_cpu, fake, self._device)
                            gradient_penalty.backward()
                            errD = errD + gradient_penalty
                            
                        self._optimizerD.step()
                        
                        if self.tensorboard != None:
                            self.tensorboard.add_scalars('train',{'Loss C real':errD_real.item(), 
                                                                'Loss C fake':errD_fake.item(),
                                                                'Loss C':errD.item()}, iters)

                            if self.Lipschitz >= 1:
                                self.tensorboard.add_scalar('train_P', gradient_penalty.item(), iters)

                            
                        pbar.update(1)
                        epochbar.update(1)
                        
                    ###############################
                    # (2) Update G network
                    #     Train the generator
                    #     every n_critic iterations
                    ###############################
                    self.netD_requires_grad_params(False)
                    # Generator update
                    # set the gradients to zero
                    self.netG.zero_grad()
                    label.fill_(self._bop['real_label'])  # fake labels are real for generator cost
                    # Generate batch of latent vectors
                    noise.resize_(b_size, self._bop['nz'], 1, 1).normal_(0, 1)
                    noisev = Variable(noise)
                    fake = self.netG(noisev)
                    output = self.netD(fake).view(-1)
                    # Calculate G's loss based on this output
                    errG = self._criterion(output, label)
                    # Calculate gradients for G
                    errG.backward()
                    # Update G
                    self._optimizerG.step()
                    gen_iterations += 1
                    
                    if self.tensorboard != None:
                        self.tensorboard.add_scalars('train',{'Loss G':errG.item()}, iters)

                    # Save Losses for plotting later
                    self._G_losses.append(errG.item())
                    self._D_losses.append(errD.item())

                    if self._to_output <= 0:
                        self.plot_losses()

                    # Output training stats
                    if (gen_iterations % num_save_iters <= 0):
                        if self._to_output >= 1:
                            print('[{:03d}/{:03d}] [{:03d}/{:03d}] [{:03d}] Loss D/G: [{:0.3f}/{:0.3f}] D_real/D_fake: [{:0.3f}/{:0.3f}] ... {}s'.format(
                                epoch + 1, 
                                self._epoch, 
                                i, 
                                data_length, 
                                gen_iterations, 
                                errD.item(), errG.item(), errD_real.item(), errD_fake.item(),
                                datetime.timedelta(seconds=round(time.time() - start_time,0))))

                        if self.tensorboard != None:
                            self.write_tensorboard_fake_images('generated fake images', 56, gen_iterations)
                            self.write_tensorboard_fake_images('generated single fake image', 1, gen_iterations)
                    
                if path_from_timestamp == "":
                    path_from_timestamp = cls_common.create_folder_datetime(self.exp_folder + self._model_path)

                # every epoch print image
                if ((epoch + 1) % 20 <= 0):
                    # appand images from generator every 5 epoch
                    self._img_list.append(self.get_fake_samples())
                    self.print_single_fake_image('Epoch {:03d}'.format(epoch + 1))
                    # save for every epoch
                    if self._autosave_mdl:
                        epoch_is_saved = True
                        self.save_fake_images(epoch + 1)
                        self.save_model(epoch + 1, path_from_timestamp + '/' + self.get_custom_file_name(epoch + 1, str(self) + '_' + str(self.netD)))
            
            if not epoch_is_saved:
                self.print_single_fake_image('Epoch {:03d}'.format(epoch + 1))
                self.save_model(self._epoch, path_from_timestamp + '/' + self.get_custom_file_name(self._epoch, str(self) + '_' + str(self.netD)))
            display("Training done....duration time {}s".format(datetime.timedelta(seconds=round(time.time() - start_time,0))))
            pbar.close()
            epochbar.close()
        finally:
            if self.tensorboard != None:
                self.tensorboard.close()