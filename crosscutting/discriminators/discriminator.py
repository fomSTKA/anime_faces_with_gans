import torch.nn as nn

class cls_critic_conv_bn_lrelu_sigmoid(nn.Module):
    def __init__(self, isize, ndf, nc, ngpu, isdropout, sigmoid = False, n_extra_layers=0):
        super(cls_critic_conv_bn_lrelu_sigmoid, self).__init__()
        my_inplace = True
        self.ngpu = ngpu
        self.isdropout = isdropout
        self.issig = sigmoid
        #nc=3
        #ndf=64
        # input is (nc) x 64 x 64
        self.conv_01 = nn.Conv2d(nc, ndf, 4, 2, 1, bias=False)
        self.l_relu_01 = nn.LeakyReLU(0.2, inplace=my_inplace)
        self.drop_01 = nn.Dropout2d(0.25)
        # state size. (ndf) x 32 x 32
        self.conv_02 = nn.Conv2d(ndf, ndf * 2, 4, 2, 1, bias=False)
        self.drop_02 = nn.Dropout2d(0.25)
        self.bn_02 = nn.BatchNorm2d(ndf * 2)
        self.l_relu_02 = nn.LeakyReLU(0.2, inplace=True)
        # state size. (ndf*2) x 16 x 16
        self.conv_03 = nn.Conv2d(ndf * 2, ndf * 4, 4, 2, 1, bias=False)
        self.drop_03 = nn.Dropout2d(0.25)
        self.bn_03 = nn.BatchNorm2d(ndf * 4)
        self.l_relu_03 = nn.LeakyReLU(0.2, inplace=True)
        # state size. (ndf*4) x 8 x 8
        self.conv_04 = nn.Conv2d(ndf * 4, ndf * 8, 4, 2, 1, bias=False)
        self.drop_04 = nn.Dropout2d(0.25)
        self.bn_04 = nn.BatchNorm2d(ndf * 8)
        self.l_relu_04 = nn.LeakyReLU(0.2, inplace=True)
        # state size. (ndf*8) x 4 x 4
        #512
        self.conv_05 = nn.Conv2d(ndf * 8, 1, 4, 1, 0, bias=False)
        self.sig = nn.Sigmoid()

    def __str__(self):
        return 'Conv_BatchN_LReLu_Sigmoid'

    def forward(self, input):
        #01
        x = self.conv_01(input)
        x = self.l_relu_01(x)
        if self.isdropout:
            x = self.drop_01(x)
        #02
        x = self.conv_02(x)
        if self.isdropout:
            x = self.drop_02(x)
        x = self.bn_02(x)
        x = self.l_relu_02(x)
        #03
        x = self.conv_03(x)
        if self.isdropout:
            x = self.drop_03(x)
        x = self.bn_03(x)
        x = self.l_relu_03(x)
        #04
        x = self.conv_04(x)
        if self.isdropout:
            x = self.drop_04(x)
        x = self.bn_04(x)
        x = self.l_relu_04(x)
        #05
        x = self.conv_05(x)
        if self.issig:
            x = self.sig(x)
        return x

    