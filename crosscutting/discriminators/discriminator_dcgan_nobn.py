import torch
import torch.nn as nn
import torch.nn.parallel

class cls_DCGAN(nn.Module):
    """Source: https://github.com/martinarjovsky/WassersteinGAN/tree/master/models"""
    def __init__(self, isize, nc, ndf, ngpu, isdropout, sigmoid = False, n_extra_layers=0):
        super(cls_DCGAN, self).__init__()
        self.ngpu = ngpu
        assert isize % 16 == 0, "isize has to be a multiple of 16"

        main = nn.Sequential()
        # input is nc x isize x isize
        # input is nc x isize x isize
        main.add_module('initial:{0}-{1}:conv'.format(nc, ndf),
                        nn.Conv2d(nc, ndf, 4, 2, 1, bias=False))
        main.add_module('initial:{0}:conv'.format(ndf),
                        nn.LeakyReLU(0.2, inplace=True))
        if isdropout:
            main.add_module('initial:{0}:dropout'.format(ndf), 
                        nn.Dropout2d(0.25))
        csize, cndf = isize / 2, ndf

        # Extra layers
        for t in range(n_extra_layers):
            main.add_module('extra-layers-{0}:{1}:conv'.format(t, cndf),
                            nn.Conv2d(cndf, cndf, 3, 1, 1, bias=False))
            main.add_module('extra-layers-{0}:{1}:relu'.format(t, cndf),
                            nn.LeakyReLU(0.2, inplace=True))
            if isdropout:
                main.add_module('initial:{0}:dropout'.format(ndf), 
                                nn.Dropout2d(0.25))

        while csize > 4:
            in_feat = cndf
            out_feat = cndf * 2
            main.add_module('pyramid:{0}-{1}:conv'.format(in_feat, out_feat),
                            nn.Conv2d(in_feat, out_feat, 4, 2, 1, bias=False))
            main.add_module('pyramid:{0}:relu'.format(out_feat),
                            nn.LeakyReLU(0.2, inplace=True))
            if isdropout:
                main.add_module('initial:{0}:dropout'.format(out_feat), 
                                nn.Dropout2d(0.25))
            cndf = cndf * 2
            csize = csize / 2

        # state size. K x 4 x 4
        main.add_module('final:{0}-{1}:conv'.format(cndf, 1),
                        nn.Conv2d(cndf, 1, 4, 1, 0, bias=False))
        if sigmoid:
            main.add_module('final:{0}-{1}:sigmoid'.format(cndf, 1),
                        nn.Sigmoid())
        self.main = main

    def __str__(self):
        return 'Conv_NoBn_LReLu'


    def forward(self, input):
        if isinstance(input.data, torch.cuda.FloatTensor) and self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        else: 
            output = self.main(input)
        return output
#        output = output.mean(0)
#        return output.view(1)