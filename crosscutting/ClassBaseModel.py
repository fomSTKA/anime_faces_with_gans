class ClsBaseModel():
    """Basic class for neuronal networks."""
    def __init__(self, netD, netG):
        #super().__init__()
        #self.__dict__.update(locals())
        self._netG = netD
        self._netD = netG
        
    @property
    def netG(self):
        return self._netG
    @netG.setter
    def netG(self, netG):
        self._netG = netG
        
    @property
    def netD(self):
        return self._netD
    @netG.setter
    def netD(self, netD):
        self._netD = netD
        
    def train(self):
        self._netD.train()
        self._netG.train()
        
    def eval(self):
        self._netD.eval()
        self._netG.eval()
    