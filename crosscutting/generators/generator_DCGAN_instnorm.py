import torch
import torch.nn as nn
import torch.nn.parallel

class cls_DCGAN(nn.Module):
    """Source: https://github.com/martinarjovsky/WassersteinGAN/tree/master/models"""
    def __init__(self, isize, nz, ngf, nc, ngpu, n_extra_layers=0):
        super(cls_DCGAN, self).__init__()
        self.ngpu = ngpu
        assert isize % 16 == 0, "isize has to be a multiple of 16"

        cngf, tisize = ngf//2, 4
        while tisize != isize:
            cngf = cngf * 2
            tisize = tisize * 2

        main = nn.Sequential()
        # input is Z, going into a convolution
        main.add_module('initial:{0}-{1}:convt'.format(nz, cngf), nn.ConvTranspose2d(nz, cngf, 4, 1, 0, bias=False))
        main.add_module('initial:{0}:instancenorm'.format(cngf), nn.InstanceNorm2d(cngf, affine = True))
        main.add_module('initial:{0}:relu'.format(cngf), nn.ReLU(True))

        csize, cndf = 4, cngf
        while csize < isize//2:
            main.add_module('pyramid:{0}-{1}:convt'.format(cngf, cngf//2), nn.ConvTranspose2d(cngf, cngf//2, 4, 2, 1, bias=False))
            main.add_module('pyramid:{0}:instancenorm'.format(cngf//2), nn.InstanceNorm2d(cngf//2, affine = True))
            main.add_module('pyramid:{0}:relu'.format(cngf//2), nn.ReLU(True))
            cngf = cngf // 2
            csize = csize * 2

        # Extra layers
        for t in range(n_extra_layers):
            main.add_module('extra-layers-{0}:{1}:conv'.format(t, cngf), nn.Conv2d(cngf, cngf, 3, 1, 1, bias=False))
            main.add_module('extra-layers-{0}:{1}:instancenorm'.format(t, cngf), nn.InstanceNorm2d(cngf, affine = True))
            main.add_module('extra-layers-{0}:{1}:relu'.format(t, cngf), nn.ReLU(True))

        main.add_module('final:{0}-{1}:convt'.format(cngf, nc), nn.ConvTranspose2d(cngf, nc, 4, 2, 1, bias=False))
        main.add_module('final:{0}:tanh'.format(nc), nn.Tanh())
        self.main = main
    def __str__(self):
        return 'Conv_InstNorm_LReLu'

    def forward(self, input):
#         if isinstance(input.data, torch.cuda.FloatTensor) and self.ngpu > 1:
#             output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
#         else: 
        output = self.main(input)
        return output