# ANIME FACES genearation with GAN's (DCGAN, WGAN with gradient penalty)

For requirements please refer to the requirements.txt. You can use that for configure your python environment.

For the training, images are required in the 'notebook_data' folder. For source images see ipynb notebook.

e.g.:

    anime_faces_with_gans
        UI
        sequences
        ...
        animefaces_WGAN.ipynb
        ...
    noetbook_data
        images
            *.jpg
            ...

Please start that project with animefaces_WGAN.ipynb notebook.
That notebook runs from top to bottom.

You can chooce different parameters to configure your generation under different conditions.

The model of the WGAN-GP with InstanceNorm performed best with Critic.

If you select "settings_config*.json" from the EXP-folder (see instructions in the notebook), you can load the model directly below.

e.g.:

    1) Load json from EXP006 folder with load button from notebook.
    2) Continue with run thruth the notebook.
    3) On notebook below you can load model (*.pt) direct.

